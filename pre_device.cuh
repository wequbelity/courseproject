#pragma once
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "constants.h"
#include "types.h"
#include <stdio.h>

void matrixMulOperation(const Matrix A, const Matrix B, Matrix dR);
void matrixCopy(Matrix &To, const Matrix &From);
void prepareDeviceMatrix(const Matrix &A, const Matrix &B, Matrix &d_A, Matrix &d_B);
void copyToDeviceMatrix(const Matrix &A, const Matrix &B, Matrix &d_A, Matrix &d_B);