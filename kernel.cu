#include "karnel.cuh"

__device__ double GetElement(const Matrix A, int row, int col) {
	return A.elements[row * A.stride + col];
}

__device__ void SetElement(Matrix A, int row, int col, double value) {
	A.elements[row * A.stride + col] = value;
}

__device__ void AddToElement(Matrix A, int row, int col, double value) {
	A.elements[row * A.stride + col] += value;
}

__device__ Matrix GetSubMatrix(Matrix A, int row, int col) {
	Matrix Asub;
	Asub.height = BLOCK_SIZE;
	Asub.width = BLOCK_SIZE;
	Asub.stride = A.stride;
	Asub.elements = &A.elements[A.stride * BLOCK_SIZE * row
		+ BLOCK_SIZE * col];
	return Asub;
}

__device__ Matrix GetSubMatrixAdvanced(Matrix A, int row, int col, int width, int height) {
	Matrix Asub;
	Asub.height = height;
	Asub.width = width;
	Asub.stride = A.stride;
	Asub.elements = &A.elements[A.stride * row
		+ col];
	return Asub;
}

__global__ void MatMulKernelFirst(const Matrix A, const Matrix B, Matrix C) {
	extern __shared__ double shared_memory[];
	int blockRow = blockIdx.y;
	int blockCol = blockIdx.x;
	int row = threadIdx.y;
	int col = threadIdx.x;

	int numberOfBlock = ((blockDim.x - 1) + (B.height - 1)) / B.height + 1;
	double *As = &shared_memory[0];
	double *Bs = &shared_memory[numberOfBlock * B.height * BLOCK_SIZE];

	int startPosition = (BLOCK_SIZE * blockCol) / B.height; // B.height == KUL
	int endPosition = (BLOCK_SIZE * (blockCol + 1) - 1) / B.height;
	int currentPosition = (BLOCK_SIZE * blockCol + col) / B.height;

	Matrix Csub = GetSubMatrix(C, blockRow, blockCol);
	Matrix Asub = GetSubMatrixAdvanced(A, 
			blockRow * BLOCK_SIZE, 
			startPosition * B.height, 
			numberOfBlock * B.height, 
			BLOCK_SIZE);
	Matrix Bsub = GetSubMatrixAdvanced(B, 0, BLOCK_SIZE * blockCol, BLOCK_SIZE, B.height);

	/*
		Заполняем shared
	*/
	int current_col = col;
	int global_pos = startPosition * B.height;
	while (current_col < numberOfBlock * B.height) {
		// if greter than last index of array
		if ((current_col + global_pos) > A.extend_width - 1) {
			As[row*numberOfBlock*B.height + current_col] = 0;
		}
		else {
			As[row*numberOfBlock*B.height + current_col] = GetElement(Asub, row, current_col);
		}
		current_col += BLOCK_SIZE;
	}
	int current_row = row;
	while (current_row <  B.height) {
		Bs[current_row * BLOCK_SIZE + col] = GetElement(Bsub, current_row, col);
		current_row += BLOCK_SIZE;
	}
	__syncthreads();

	double Cvalue = 0;
	int colum_index = (currentPosition - startPosition);
	int as_num = row * numberOfBlock*B.height + B.height * colum_index;
	for (int m = 0; m < B.height; ++m) {
		Cvalue += As[as_num + m] * Bs[BLOCK_SIZE*m + col];
	}
	SetElement(Csub, row, col, Cvalue);
}

__global__ void MatMulKernelSecond(const Matrix A, const Matrix B, Matrix C) {
	int blockRow = blockIdx.y;
	int blockCol = blockIdx.x;

	Matrix Csub = GetSubMatrix(C, blockRow, blockCol);

	double Cvalue = 0;

	int row = threadIdx.y;
	int col = threadIdx.x;

	for (int m = 0; m < (A.extend_width / BLOCK_SIZE); ++m) {
		Matrix Asub = GetSubMatrix(A, blockRow, m);
		Matrix Bsub = GetSubMatrix(B, blockCol, m);

		__shared__ double As[BLOCK_SIZE][BLOCK_SIZE];
		__shared__ double Bs[BLOCK_SIZE][BLOCK_SIZE];

		As[row][col] = GetElement(Asub, row, col);
		Bs[col][row] = GetElement(Bsub, row, col);

		__syncthreads();

		for (int e = 0; e < BLOCK_SIZE; ++e) {
			Cvalue += As[row][e] * Bs[e][col];
		}

		__syncthreads();
	}
	AddToElement(Csub, row, col, Cvalue);
}
