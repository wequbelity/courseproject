#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <Windows.h>
#include "pre_device.cuh"
#include "matrix_utils.h"
#include "types.h"

#include <windows.h>
#include <sys/timeb.h>
#include <time.h>

void solution();
void printResultFile();

int main() {
	struct _timeb timebufer;
	double start_time, end_time, solution_time;
	
	printResultFile();
	system("pause");

	_ftime(&timebufer);
	start_time = double(timebufer.time) + 0.001*timebufer.millitm;
	solution();
	_ftime(&timebufer);
	end_time = double(timebufer.time) + 0.001*timebufer.millitm;
	solution_time = end_time - start_time;
	printf("Solution time: %f\n", solution_time);

	system("pause");

	return 0;
	
}

void makeMatrixOfElement(Matrix &A, double *data, int size, size_t step) {
	int i, j, k;
	double *chunk = A.elements;
	int shift = size * step;
	for (i = 0, k = 0; i < size; ++i) {
		for (j = i; j < size; j++, k++) {
			if (i == j) {
				chunk[i * A.stride + j + shift] = data[k];
				continue;
			}
			chunk[i + j * A.stride + shift] = data[k];
			chunk[i * A.stride + j + shift] = data[k];
		}
	}
}

/**
*	������������ ����������
*/
void makeSubUF(Matrix &A, double **UF, const int *nur, size_t size, size_t step) {
	int i, j;
	size_t shift = step * size;
	for (i = 0; i < A.height; ++i) {
		for (j = 0; j < size; ++j) {
			A.elements[i * A.stride + j + shift] = UF[i][nur[j]];
		}
	}
}


/**
*	����� �� ����� ��������������� �����
*/
void printResultFile() {
	FILE *fpnf;
	char name2[40];
	sprintf_s(name2, "%s\\%s\0", FOLDER, "Reduced_Stiffnes.bin");
	fpnf = fopen(name2, "rb");

	int nform, i;
	double **KK;

	fread(&nform, sizeof(int), 1, fpnf);
	KK = (double**)calloc(nform, sizeof(double*));
	for (i = 0; i < nform; i++) {
		KK[i] = (double*)calloc(nform, sizeof(double));
		fread(KK[i], sizeof(double), nform, fpnf);
	}
	fclose(fpnf);

	int count = 1;
	for (i = 0; i < nform; i++) {
		printf("%d| ", count++); 
		for (int j = 0; j < nform; ++j) {
			printf("%f ", KK[i][j]);
		}
		printf("\n");
	}
	for (int i = 0; i < nform; ++i) {
		free(KK[i]);
	}
	free(KK);
}

/**
*	�������������� �������.
*	���������� ������;
*	������������ � ������������� ������; 
*	������ ����_��������� �������.
*/
void solution() {
	
	Matrix A;
	Matrix B; 
	Matrix C;
	Matrix d_A, d_B;

	size_t step = 0; // �������������� ���. �������� �� ������������ ������ ������

	FILE *fpelmat;
	char name[40]; // ��� �����
	int nfullelmat = 0; // ���������� �������� ���������
	int nstel; // ���������� ��������� � ����� ������� ��������� ������� ���������
	int *NUR; // ������ ������� ���������
	int KUL; // ����������� ������� ������� ���������
	double *GEL; // ��������� ������� ���������

	FILE *fpnf;
	int nform; 
	int	nur;
	double **UF;

	sprintf(name, "%s\\%s\0", FOLDER, "Eigen_forms.bin");
	fpnf = fopen(name, "rb");
	fread(&nform, sizeof(int), 1, fpnf);
	fread(&nur, sizeof(int), 1, fpnf);
	UF = (double**)malloc(nform*(sizeof(double*)));
	for (int i = 0; i<nform; i++) {
		UF[i] = (double*)calloc(nur, sizeof(double));
		fread(UF[i], sizeof(double), nur, fpnf);
	}
	fclose(fpnf);

	sprintf(name, "%s\\%s\0", FOLDER, "StifElmat.bin");
	fpelmat = fopen(name, "rb");
	fread(&nfullelmat, sizeof(int), 1, fpelmat);
	fread(&KUL, sizeof(int), 1, fpelmat);

	makeMatrix(A, KUL, KUL * NUM);
	makeMatrix(B, nform, KUL * NUM);
	makeMatrix(C, nform, nform);

	Matrix d_C;
	matrixCopy(d_C, C);
	size_t size = C.extend_height * C.extend_width * sizeof(double);
	cudaMalloc(&d_C.elements, size);
	cudaMemset(d_C.elements, 0, size);

/** ���� ���������� - ������ **/
	nstel = int((KUL*KUL - KUL) / 2 + 0.1) + KUL; 
	NUR = (int*)malloc(KUL * sizeof(int));
	GEL = (double*)malloc(nstel * sizeof(double));

	fread(NUR, sizeof(int), KUL, fpelmat);
	fread(GEL, sizeof(double), nstel, fpelmat);

	makeMatrixOfElement(A, GEL, KUL, step);
	makeSubUF(B, UF, NUR, KUL, step);
	step++;

	prepareDeviceMatrix(A, B, d_A, d_B);
	copyToDeviceMatrix(A, B, d_A, d_B);
	for (int j = 1; j<nfullelmat; j++) {
		if (j % (nfullelmat / 10) == 0) {
			printf(".");
		}
		if (step == NUM) {
			copyToDeviceMatrix(A, B, d_A, d_B);
			matrixMulOperation(d_B, d_A, d_C);
			step = 0;
			if ((nfullelmat - j) < NUM) {
				memset(A.elements, 0, A.extend_width*A.extend_height*sizeof(double));
				memset(A.elements, 0, A.extend_width*A.extend_height*sizeof(double));
			}
		}
		fread(&KUL, sizeof(int), 1, fpelmat);
		fread(NUR, sizeof(int), KUL, fpelmat);
		fread(GEL, sizeof(double), nstel, fpelmat);
		makeMatrixOfElement(A, GEL, KUL, step);
		makeSubUF(B, UF, NUR, KUL, step);
		step++;
	}
	fclose(fpelmat);
	copyToDeviceMatrix(A, B, d_A, d_B);
	matrixMulOperation(d_B, d_A, d_C);

	cudaMemcpy(C.elements, d_C.elements, size, cudaMemcpyDeviceToHost);
	cudaFree(d_A.elements);
	cudaFree(d_B.elements);
	cudaFree(d_C.elements);


/** ���� ���������� - ����� **/
	printExtendMatrix(C);

	free(A.elements);
	free(B.elements);
	free(C.elements);

	free(NUR);
	free(GEL);
	for (int i = 0; i < nform; ++i) {
		free(UF[i]);
	}
	free(UF);
}