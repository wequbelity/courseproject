#pragma once
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "constants.h"
#include "types.h"

__device__ double GetElement(const Matrix A, int row, int col);
__device__ void SetElement(Matrix A, int row, int col, double value);
__device__ void AddToElement(Matrix A, int row, int col, double value);
__device__ Matrix GetSubMatrix(Matrix A, int row, int col);
__device__ Matrix GetSubMatrixAdvanced(Matrix A, int row, int col, int width, int height);
__global__ void MatMulKernelFirst(const Matrix A, const Matrix B, Matrix C);
__global__ void MatMulKernelSecond(const Matrix A, const Matrix B, Matrix C);
