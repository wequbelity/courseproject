#pragma once
#include "types.h"

double getElement(const Matrix &A, int row, int col);
void setElement(Matrix A, int row, int col, double value);
void printExtendMatrix(const Matrix &A);
void printMatrix(const Matrix &A);
void initByValue(Matrix &A, double value);
void makeMatrix(Matrix &A, int height, int width);