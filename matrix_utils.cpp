#include "matrix_utils.h"
#include "constants.h"
#include <stdlib.h>
#include <stdio.h>

double getElement(const Matrix &A, int row, int col) {
	return A.elements[row * A.stride + col];
}

void setElement(Matrix A, int row, int col, double value) {
	A.elements[row * A.stride + col] = value;
}

void makeMatrix(Matrix &A, int height, int width) {
	A.width = width;
	A.height = height;
	A.extend_height = ((height + (BLOCK_SIZE - 1)) / BLOCK_SIZE)*BLOCK_SIZE;
	A.extend_width = ((width + (BLOCK_SIZE - 1)) / BLOCK_SIZE)*BLOCK_SIZE;;
	A.stride = A.extend_width;
	A.elements = (double*)calloc(A.extend_height * A.extend_width, sizeof(double));
}

void printExtendMatrix(const Matrix &A) {
	int i = 0;
	int count = 1;
	printf("Data of matrix: width = %d, height = %d, stride = %d\n", A.width, A.height, A.stride);
	printf("__________________________________________________________________________\n");
	int j = 1;
	printf("%d|  ", j);
	for (i; i < A.extend_height * A.extend_width; ++i) {
		printf("%1.10f", A.elements[i]);
		printf(" ");
		if ((i + 1) % A.extend_width == 0) {
			printf("\n");
			j++;
			printf("%d|  ", j);
		}
	}
	printf("\n___________________________________________________________________________\n");
}

void printMatrix(const Matrix &A) {
	int count = 1;
	int i, j;
	printf("Data of matrix: width = %d, height = %d, stride = %d\n", A.width, A.height, A.stride);
	printf("__________________________________________________________________________\n");
	for (i = 0; i < A.height; ++i) {
		for (j = 0; j < A.width; ++j) {
			//if (count % 4 == 0) printf("\n");
			printf("%f ", getElement(A, i, j));
			count++;
		}
		printf("\n");
	}
	printf("___________________________________________________________________________\n");
}

void initByValue(Matrix &A, double value) {
	int i, j;
	for (i = 0; i < A.height; ++i) {
		for (j = 0; j < A.width; ++j) {
			setElement(A, i, j, value);
		}
	}
}