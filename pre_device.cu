#include "pre_device.cuh"
#include "karnel.cuh"

void matrixCopy(Matrix &To, const Matrix &From) {
	To.extend_height = From.extend_height;
	To.extend_width = From.extend_width;
	To.width = From.width;
	To.height = From.height;
	To.stride = From.stride;
}

void prepareDeviceMatrix(const Matrix &A, const Matrix &B, Matrix &d_A, Matrix &d_B) {
	size_t size = 0;

	matrixCopy(d_A, A);
	size = A.extend_width * A.extend_height * sizeof(double);
	cudaMalloc(&d_A.elements, size);
	

	matrixCopy(d_B, B);
	size = B.extend_width * B.extend_height * sizeof(double);
	cudaMalloc(&d_B.elements, size);

}

void copyToDeviceMatrix(const Matrix &A, const Matrix &B, Matrix &d_A, Matrix &d_B) {
	size_t size = 0;
	size = A.extend_width * A.extend_height * sizeof(double);
	cudaMemcpy(d_B.elements, B.elements, size,
		cudaMemcpyHostToDevice);
	size = B.extend_width * B.extend_height * sizeof(double);
	cudaMemcpy(d_A.elements, A.elements, size,
		cudaMemcpyHostToDevice);
}
void matrixMulOperation(const Matrix d_A, const Matrix d_B, Matrix dR) {
	size_t size = 0;

	Matrix d_Temp;
	d_Temp.width = d_B.width;
	d_Temp.height = d_A.height;
	d_Temp.extend_height = d_A.extend_height;
	d_Temp.extend_width = d_B.extend_width;
	d_Temp.stride = d_B.stride;

	size = d_Temp.extend_width * d_Temp.extend_height * sizeof(double);
	cudaMalloc(&d_Temp.elements, size);

	dim3 dimBlock(BLOCK_SIZE, BLOCK_SIZE);

	dim3 dimGrid((d_Temp.extend_width + dimBlock.x - 1) / dimBlock.x, (d_Temp.extend_height + dimBlock.y - 1) / dimBlock.y);

#ifdef DEV_ERROR
	cudaError_t error = cudaGetLastError();
	printf("Before Karnel run: %s\n", cudaGetErrorString(error));
#endif

	int numberOfBlock = ((dimBlock.x - 1) + (d_B.height - 1)) / d_B.height + 1;
	MatMulKernelFirst <<<
		dimGrid, dimBlock, (numberOfBlock*d_B.height*BLOCK_SIZE + d_B.height*BLOCK_SIZE)*sizeof(double)
		>>>(d_A, d_B, d_Temp);
	cudaDeviceSynchronize();

	dim3 dimBlock2(BLOCK_SIZE, BLOCK_SIZE);
	dim3 dimGrid2((dR.extend_width + dimBlock.x - 1) / dimBlock.x, (dR.extend_height + dimBlock.y - 1) / dimBlock.y);

	MatMulKernelSecond << < dimGrid2, dimBlock2 >> >(d_Temp, d_A, dR);
	cudaDeviceSynchronize();

#ifdef DEV_ERROR
	error = cudaGetLastError();
	printf("Karnel run: %s\n", cudaGetErrorString(error));
#endif

	cudaFree(d_Temp.elements);
}